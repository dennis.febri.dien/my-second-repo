from django.shortcuts import render, redirect

import datetime


# Enter your name here
mhs_name = 'Dennis Febri Dien' # TODO Implement this
birth_date = datetime.date(1998,2,25) # year,month,day


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(int(birth_date.strftime("%Y")))}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year): #birth_year must be datetime.date data type
    now = datetime.date.today()
    return int(now.strftime("%Y"))-birth_year

