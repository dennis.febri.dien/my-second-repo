

function appendText() {
    var txt1 = $('textarea#input-chat').val();            // Create element with HTML  
	var endline = "\n";
    $("div#msg-insert").append(txt1, endline);      // Append the new elements 
}

var erase = false;
var go = function(x) {
var print = document.getElementById('print');
  if (erase==true){
	  print.value="";
	  erase = false;
  }
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

$('.apply-button-class').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada

    // [TODO] ambil object theme yang dipilih

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya

    // [TODO] simpan object theme tadi ke local storage selectedTheme
})



function evil(fn) {
  return new Function('return ' + fn)();
}

$(document).ready(function() {
	
	 var button_8 = $('button:contains("8")');
    var button_4 = $('button:contains("4")');

    var button_add = $('button:contains("+")');
    var button_sub = $('button:contains("-")');
    var button_mul = $('button:contains("*")');
    var button_div = $('button:contains("/")');

    var button_clear = $('button:contains("AC")');
    var button_res = $('button:contains("=")');

    QUnit.test( "Addition Test", function( assert ) {
      button_8.click();
      button_add.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
      button_clear.click();
    });

	 QUnit.test( "Substraction Test", function( assert ) {
      button_8.click();
      button_sub.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 4, "8 - 4 must be 4" );
      button_clear.click();
    });
	
	QUnit.test( "Multiply Test", function( assert ) {
  button_8.click();
  button_mul.click();
  button_4.click();
  button_res.click();
  assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
  button_clear.click();
});

QUnit.test( "Division Test", function( assert ) {
  button_8.click();
  button_div.click();
  button_4.click();
  button_res.click();
  assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
  button_clear.click();
});



	
	$('textarea#input-chat').keypress(function(e){
	  if(e.keyCode == 13 && !e.shiftKey) {
	   e.preventDefault();
	    var txt1 = $('textarea#input-chat').val() +"<br>";            // Create element with HTML  
		
		$("div.msg-insert").append(txt1);
		document.getElementById("input-chat").value = "";
	  }
	  
	  
});




	var themes = [{"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
	{"id":1,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":2,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":3,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
  
  //default theme jika web pertama kali dibuka
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  //simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));
	
  //menload tema ke select2
	
  var retrievedObject = localStorage.getItem('themes');
  $('.my-select').select2({data: JSON.parse(retrievedObject)});
	
  //menerapkan selectedTheme yang ada di local storage
  var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
  var key;
  var bcgColor;
  var fontColor;
	for (key in retrievedSelected) {
    	if (retrievedSelected.hasOwnProperty(key)) {
        	bcgColor=retrievedSelected[key].bcgColor;
        	fontColor=retrievedSelected[key].fontColor;
    	}
	}  
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


  //fungsi tombol apply
	$('.apply-button').on('click', function(){  // sesuaikan class button

    var valueTheme = $('.my-select').val();
    var theme;
    var a;
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});

});



