from django.db import models
from datetime import datetime
class Message(models.Model):
	name = models.CharField(max_length=27)
	email = models.EmailField()
	message = models.TextField()
	created_date = models.DateTimeField(default=datetime.now())
	
	#def __str__(self):
	#	return self.message
# Create your models here.
