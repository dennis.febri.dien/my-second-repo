# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-15 15:52
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0005_auto_20171113_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 15, 22, 52, 45, 956532)),
        ),
    ]
