from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Aku adalah mahasiswa fasilkom UI 2016, hobiku adalah robotika dan mempelajari hal yang baru. contohnya adalah mempelajari Pembelajaran Pemrograman Web.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
