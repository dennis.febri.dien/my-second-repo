from django.test import TestCase,Client
from django.urls import resolve
from .models import Friend
from .views import index,add_friend,validate_npm,delete_friend,friend_list,friend_list_json,get_next_prev
from .api_csui_helper.csui_helper import CSUIhelper 

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_json_using_index_func(self):
        found = resolve('/lab-7/friend-list/')
        self.assertEqual(found.func, friend_list)
	
    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_json_using_index_func(self):
        found = resolve('/lab-7/get-friend-list/')
        self.assertEqual(found.func, friend_list_json)
		
    def test_lab7_post_success_next_prev(self):
        response_post = Client().post('/lab-7/get-next-prev', {'type': 'next', 'pg': 3})
        self.assertEqual(response_post.status_code, 301)
        response_post = Client().post('/lab-7/get-next-prev', {'type': 'prev', 'pg': 2})
        self.assertEqual(response_post.status_code, 301)
		
    def test_lab_7_get_friend_list_json_response(self):
        response = Client().get('/lab-7/get-friend-list/')
        friends = [obj.as_dict() for obj in Friend.objects.all()]
        self.assertJSONEqual(str(response.content, encoding='utf8'),{'friend_list': friends})

	
# Create your tests here.
