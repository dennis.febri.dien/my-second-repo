from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    #next = csui_helper.instance.get_next_mahasiswa_list()
    #prev = csui_helper.instance.get_prev_mahasiswa_list()
    access_token =  csui_helper.instance.get_access_token()
    client_id = csui_helper.instance.get_client_id()
    #auth = csui_helper.instance.get_auth_param_dict()
    
    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list,"author":"Dennis Febri Dien","at":access_token,"ci":client_id}
    html = 'lab_7/lab_7.html'
	
    return render(request, html, response)

@csrf_exempt
def friend_list_json(request):
	return JsonResponse({"friend_list":list(Friend.objects.values())})

@csrf_exempt
def get_next_prev(request):#pragma: no cover
    if request.method == 'POST':
        type = request.POST['type']
        pg = int(request.POST['pg'])
        link = csui_helper.instance.get_API_link()
        if(type=="previous"):
            pg -= 1
            if(pg!=1):
                link+= ("&page="+str(pg))
            
           
        else:
            pg+=1
            link+= ("&page="+(str(pg)))
        
        mahasiswa_list =  json.dumps(csui_helper.instance.get_list(link))
        
        return HttpResponse(mahasiswa_list,content_type="application/json")
    return
	
def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)
	
	
@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        
        if Friend.objects.all().filter(npm=friend.npm).exists():
            data = {'is_taken': Friend.objects.filter(npm__exact=npm).exists()}
            return JsonResponse(data)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data, content_type="application/json")

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__exact=npm).exists() #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
		}
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data


